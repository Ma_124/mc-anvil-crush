<!-- README.md is generated using `gradle updateReadme` -->

# Anvil Crush
Additional carpet rules.

- Download builds on [Modrinth](https://modrinth.com/mod/anvil-crush).
- Source code on [GitLab](https://gitlab.com/Ma_124/mc-anvil-crush).

Depends on [fabric-carpet](https://github.com/gnembon/fabric-carpet) for the same minecraft version.
