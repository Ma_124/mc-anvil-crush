<!-- README.md is generated using `gradle updateReadme` -->

# Anvil Crush
Additional carpet rules.

- Download builds on [Modrinth](https://modrinth.com/mod/anvil-crush).
- Source code on [GitLab](https://gitlab.com/Ma_124/mc-anvil-crush).

Depends on [fabric-carpet](https://github.com/gnembon/fabric-carpet) for the same minecraft version.

## Anvil Crush Settings
### anvilCrushIce
falling anvils crush ice  
frosted ice -> ice -> packed ice -> blue ice  
* Type: `Boolean`  
* Default value: `false`  
* Required options: `true`, `false`  
* Categories: `MA_124`, `ANVIL`, `SURVIVAL`, `EXPERIMENTAL`, `FEATURE`  

### anvilCrushStone
falling anvils crush stony types  
cobblestone -> gravel -> sand  
* Type: `Boolean`  
* Default value: `false`  
* Required options: `true`, `false`  
* Categories: `MA_124`, `ANVIL`, `SURVIVAL`, `EXPERIMENTAL`, `FEATURE`  

### anvilFallingDamage
falling anvils take damage  

* Type: `Boolean`  
* Default value: `true`  
* Required options: `true`, `false`  
* Categories: `MA_124`, `ANVIL`, `SURVIVAL`, `EXPERIMENTAL`  

### anvilTooExpensive
disable 'too expensive' on anvils  
the UI will still show the error  
* Type: `Boolean`  
* Default value: `true`  
* Required options: `true`, `false`  
* Categories: `MA_124`, `SURVIVAL`, `ANVIL`  

### axesAreWeapons
treat axes as weapons (e.g. allow looting in survival)  

* Type: `Boolean`  
* Default value: `false`  
* Required options: `true`, `false`  
* Categories: `MA_124`, `SURVIVAL`  

### canLeashVillager
villagers can be leashed  

* Type: `Boolean`  
* Default value: `false`  
* Required options: `true`, `false`  
* Categories: `MA_124`, `SURVIVAL`  

### clickThroughItemFrame
click through item frames that are invisible and fixed  

* Type: `Boolean`  
* Default value: `false`  
* Required options: `true`, `false`  
* Categories: `MA_124`, `BUGFIX`  

### compostPoisonousPotatoes
compostable poisonous potatoes  
changes require server restart or /reload  
* Type: `Boolean`  
* Default value: `false`  
* Required options: `true`, `false`  
* Categories: `MA_124`, `SURVIVAL`  

### compostRottenFlesh
compostable rotten flesh  
changes require server restart or /reload  
* Type: `Boolean`  
* Default value: `false`  
* Required options: `true`, `false`  
* Categories: `MA_124`, `SURVIVAL`  

### keepXP
keep experience points on death  

* Type: `Boolean`  
* Default value: `false`  
* Required options: `true`, `false`  
* Categories: `MA_124`, `SURVIVAL`  

### noExplosionDamage
players don't get hurt by explosions  

* Type: `Boolean`  
* Default value: `false`  
* Required options: `true`, `false`  
* Categories: `MA_124`, `SURVIVAL`, `DAMAGE`  

### noFireDamage
players don't get hurt by fire  
specifically applies to fire, lava, and magma blocks  
* Type: `Boolean`  
* Default value: `false`  
* Required options: `true`, `false`  
* Categories: `MA_124`, `SURVIVAL`, `DAMAGE`  

### noKinecticDamage
players don't get hurt by flying into walls  

* Type: `Boolean`  
* Default value: `false`  
* Required options: `true`, `false`  
* Categories: `MA_124`, `SURVIVAL`, `DAMAGE`  

### noPortalTicking
zombified piglin no long spawn in nether portals  

* Type: `Boolean`  
* Default value: `false`  
* Required options: `true`, `false`  
* Categories: `MA_124`, `SURVIVAL`, `CREATIVE`  

### nonOPWhereIs
enable /where{is,ami}, /imhere, /{he,she}is for non operators  

* Type: `Boolean`  
* Default value: `false`  
* Required options: `true`, `false`  
* Categories: `MA_124`, `SURVIVAL`, `CREATIVE`  

### printDeathCoords
print coordinates in player death messages  

* Type: `Boolean`  
* Default value: `false`  
* Required options: `true`, `false`  
* Categories: `MA_124`, `SURVIVAL`  

### shiftClickItemFrameInvisible
shift + use (right mouse) makes item frames invisible and fixed  

* Type: `Boolean`  
* Default value: `false`  
* Required options: `true`, `false`  
* Categories: `MA_124`, `CREATIVE`, `SURVIVAL`, `FEATURE`  

### villagerLockTrades
villager lock trades.  
this can permanently alter the behaviour of any villages traded with while this rule was activated  
* Type: `Boolean`  
* Default value: `true`  
* Required options: `true`, `false`  
* Categories: `MA_124`, `EXPERIMENTAL`, `SURVIVAL`  

## Anvil Crush Commands
### /analyze \<radius\>
For each layer in the world (in the given radius) output how many blocks were found on that height
to `/tmp/mc-results.csv`.

Example excerpt from `/tmp/mc-results.csv`:

```csv
block.minecraft.redstone_ore,10,11423
block.minecraft.water,10,265
block.minecraft.deepslate_diamond_ore,10,298
block.minecraft.copper_ore,10,1250
block.minecraft.deepslate_iron_ore,10,2928
block.minecraft.coal_ore,10,14556
block.minecraft.diamond_ore,10,1540
block.minecraft.redstone_ore,11,11549
block.minecraft.water,11,1191
block.minecraft.deepslate_diamond_ore,11,297
block.minecraft.copper_ore,11,1227
block.minecraft.deepslate_iron_ore,11,2954
block.minecraft.coal_ore,11,14361
block.minecraft.diamond_ore,11,1530
```

## Build Mod for different MC versions
Download the repository
- using git: `git clone https://gitlab.com/Ma_124/mc-anvil-crush.git`.
- as a [.zip file](https://gitlab.com/Ma_124/mc-anvil-crush/-/archive/master/mc-anvil-crush-master.zip).

If you want to build the mod yourself for a different Minecraft version you need to change the following files:
- [`src/main/resources/fabric.mod.json`](./src/main/resources/fabric.mod.json): Change `depends.minecraft` to new version.
- [`gradle.properties`](./gradle.properties)
  - Update the properties under `# Fabric Properties` with the values found [here](https://fabricmc.net/versions.html).
  - Update to the most recent carpet version for your release. A list of all carpet versions can be found [here](https://masa.dy.fi/maven/carpet/fabric-carpet).
- [`build.gradle`](./build.gradle): Under the `publishModrinth` task change `addGameVersion` to the appropriate version(s).

You may have to change the Java version which can be done at the following places:
- [`build.gradle`](./build.gradle):
  - Change `sourceCompatibility` and `targetCompatibility`.
  - Change `it.options.release` in `tasks.withType(JavaCompile)`.
- [`./src/main/resources/anvil-crush.mixins.json`](./src/main/resources/anvil-crush.mixins.json): Change `compatibilityLevel`.
