package technicalma.carpet.anvilcrush.mixins;

import net.minecraft.item.MiningToolItem;
import net.minecraft.item.ToolItem;
import net.minecraft.item.ToolMaterial;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.ModifyConstant;
import technicalma.carpet.anvilcrush.helpers.AxeEnchantmentHelper;

@Mixin(MiningToolItem.class)
public abstract class MiningToolItemMixin extends ToolItem {
    public MiningToolItemMixin(ToolMaterial material, Settings settings) {
        super(material, settings);
    }

    @ModifyConstant(method = "postHit", constant = @Constant(intValue = 2))
    private int toolDamageAmount(int orig) {
        if (AxeEnchantmentHelper.isWeaponAxe(this))
            return 1;
        else
            return orig;
    }
}
