package technicalma.carpet.anvilcrush.mixins;

import net.minecraft.entity.damage.DamageTracker;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import technicalma.carpet.anvilcrush.AnvilCrushSettings;

@Mixin(DamageTracker.class)
public abstract class DamageTrackerMixin {
    @Inject(method = "getDeathMessage", at = @At("RETURN"), cancellable = true)
    private void getDeathMessage(CallbackInfoReturnable<Text> cir) {
        if (AnvilCrushSettings.printDeathCoords) {
            try {
                var orig = cir.getReturnValue();
                var pos = ((DamageTracker) (Object) this).getEntity().getBlockPos();
                var coords = pos.getX() + " " + pos.getY() + " " + pos.getZ();

                cir.setReturnValue(((MutableText) orig).append(" (at " + coords + ")"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
