package technicalma.carpet.anvilcrush.mixins;

import net.minecraft.screen.AnvilScreenHandler;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.ModifyConstant;
import technicalma.carpet.anvilcrush.AnvilCrushSettings;

@Mixin(AnvilScreenHandler.class)
public abstract class AnvilScreenHandlerMixin {
    // disable the if statement below (AnvilScreenHandler.java:254 in yarn-18+build.1)
    //   if (this.levelCost.get() >= 40 && !this.player.getAbilities().creativeMode) {...}
    @ModifyConstant(method = "updateResult", constant = @Constant(intValue = 40, ordinal = 2))
    private int maxLevelCost(int orig) {
        if (AnvilCrushSettings.anvilTooExpensive)
            return orig;
        else
            return Integer.MAX_VALUE;
    }
}
