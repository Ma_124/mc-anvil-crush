package technicalma.carpet.anvilcrush.mixins;

import net.minecraft.block.Block;
import net.minecraft.block.ComposterBlock;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import technicalma.carpet.anvilcrush.helpers.CompostHelper;

@Mixin(ComposterBlock.class)
public abstract class ComposterBlockMixin extends Block {
    public ComposterBlockMixin(Settings settings) {
        super(settings);
    }

    @Inject(at = @At("TAIL"), method = "registerDefaultCompostableItems()V")
    private static void registerDefaultCompostableItems(CallbackInfo cbi) {
        CompostHelper.updateCompostables();
    }
}
