package technicalma.carpet.anvilcrush.mixins;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentLevelEntry;
import net.minecraft.item.ItemStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import technicalma.carpet.anvilcrush.helpers.AxeEnchantmentHelper;

import java.util.List;

@Mixin(EnchantmentHelper.class)
public abstract class EnchantmentHelperMixin {
    @Inject(method = "getPossibleEntries", at = @At("RETURN"), cancellable = true)
    private static void getPossibleEntries(int power, ItemStack stack, boolean treasureAllowed, CallbackInfoReturnable<List<EnchantmentLevelEntry>> cir) {
        if (AxeEnchantmentHelper.isWeaponAxe(stack.getItem())) {
            var entries = cir.getReturnValue();

            for (var enchantment : AxeEnchantmentHelper.WEAPON_ENCHANTMENTS) {
                for (int level = enchantment.getMaxLevel(); level >= enchantment.getMinLevel(); level--) {
                    if (enchantment.getMinPower(level) <= power && power <= enchantment.getMaxPower(level)) {
                        entries.add(new EnchantmentLevelEntry(enchantment, level));
                        break;
                    }
                }
            }

            cir.setReturnValue(entries);
        }
    }
}
