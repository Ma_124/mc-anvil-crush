package technicalma.carpet.anvilcrush.mixins;

import net.minecraft.entity.decoration.ItemFrameEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(ItemFrameEntity.class)
public interface ItemFrameEntityAccessor {
    @Accessor
    boolean isFixed();

    @Accessor
    void setFixed(boolean b);
}
