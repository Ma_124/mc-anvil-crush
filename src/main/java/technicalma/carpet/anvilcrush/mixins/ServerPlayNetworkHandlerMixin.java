package technicalma.carpet.anvilcrush.mixins;

import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.LockableContainerBlockEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.decoration.ItemFrameEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.server.network.ServerPlayNetworkHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.EntityHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import technicalma.carpet.anvilcrush.AnvilCrushSettings;

// Anonymous class <implements Handler> in ServerPlayNetworkHandler$1.onPlayerInteractEntity (ServerPlayNetworkHandler.java:1231 in yarn-18+build.1)
@Mixin(targets = "net/minecraft/server/network/ServerPlayNetworkHandler$1")
public abstract class ServerPlayNetworkHandlerMixin {
    @Shadow
    @Final
    ServerPlayNetworkHandler field_28963;

    @Shadow
    @Final
    Entity field_28962;

    @Inject(method = "interactAt(Lnet/minecraft/util/Hand;Lnet/minecraft/util/math/Vec3d;)V", at = @At(value = "HEAD"), cancellable = true)
    public void onPlayerInteractEntity(Hand hand, Vec3d hitPosition, CallbackInfo ci) {
        if (AnvilCrushSettings.clickThroughItemFrame || AnvilCrushSettings.shiftClickItemFrameInvisible) {
            PlayerEntity player = field_28963.player;
            World world = player.getEntityWorld();

            EntityHitResult hitResult = new EntityHitResult(field_28962, hitPosition.add(field_28962.getX(), field_28962.getY(), field_28962.getZ()));

            if (hitResult.getEntity() instanceof ItemFrameEntity itemFrame) {
                var itemFrameAcc = ((ItemFrameEntityAccessor) itemFrame);

                if (player.isSneaking()) {
                    if (AnvilCrushSettings.shiftClickItemFrameInvisible) {
                        if (itemFrame.isInvisibleTo(player) || itemFrameAcc.isFixed()) {
                            itemFrame.setInvisible(false);
                            itemFrameAcc.setFixed(false);
                            player.sendMessage(Text.of("Made item frame visible."), true);
                            ci.cancel();
                        } else if (itemFrame.getHeldItemStack() != ItemStack.EMPTY) {
                            itemFrame.setInvisible(true);
                            itemFrameAcc.setFixed(true);
                            player.sendMessage(Text.of("Made item frame invisible."), true);
                            ci.cancel();
                        }
                    }
                } else {
                    if (AnvilCrushSettings.clickThroughItemFrame) {
                        if (itemFrame.isInvisibleTo(player) && itemFrameAcc.isFixed()) {
                            BlockPos attachedTo = itemFrame.getDecorationBlockPos().offset(itemFrame.getHorizontalFacing().getOpposite());
                            BlockEntity attachedBlockEntity = world.getBlockEntity(attachedTo);

                            if (attachedBlockEntity instanceof LockableContainerBlockEntity) {
                                //noinspection CastCanBeRemovedNarrowingVariableType
                                field_28963.getPlayer().interactionManager.interactBlock(
                                        (ServerPlayerEntity) player,
                                        world,
                                        player.getStackInHand(hand),
                                        hand,
                                        new BlockHitResult(hitPosition, itemFrame.getHorizontalFacing(), attachedTo, false)
                                );
                                ci.cancel();
                            }
                        }
                    }
                }
            }
        }
    }
}
