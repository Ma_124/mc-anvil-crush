package technicalma.carpet.anvilcrush.mixins;

import net.minecraft.village.TradeOffer;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import technicalma.carpet.anvilcrush.AnvilCrushSettings;

@Mixin(TradeOffer.class)
public abstract class TradeOfferMixin {
    @Shadow
    private @Final
    @Mutable
    int maxUses;
    @Shadow
    private int demandBonus;

    @Inject(method = "<init>*", at = @At("RETURN"))
    private void init(CallbackInfo ci) {
        if (!AnvilCrushSettings.villagerLockTrades) {
            this.maxUses = Integer.MAX_VALUE;
            this.demandBonus = 0;
        }
    }

    @Inject(method = "updateDemandBonus", at = @At("RETURN"))
    private void updateDemandBonus(CallbackInfo ci) {
        if (!AnvilCrushSettings.villagerLockTrades) {
            this.demandBonus = 0;
        }
    }
}
