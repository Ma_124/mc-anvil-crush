package technicalma.carpet.anvilcrush.mixins;

import com.google.common.collect.ImmutableMap;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.FallingBlockEntity;
import net.minecraft.tag.BlockTags;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;
import technicalma.carpet.anvilcrush.AnvilCrushSettings;

import java.util.Map;

@Mixin(FallingBlockEntity.class)
public abstract class FallingBlockEntityMixin extends Entity {
    @Shadow
    public abstract BlockState getBlockState();

    private final Map<Block, Block> crushStone = ImmutableMap.of(
            Blocks.COBBLESTONE, Blocks.GRAVEL,
            Blocks.GRAVEL, Blocks.SAND
    );

    private final Map<Block, Block> crushIce = ImmutableMap.of(
            Blocks.FROSTED_ICE, Blocks.ICE,
            Blocks.ICE, Blocks.PACKED_ICE,
            Blocks.PACKED_ICE, Blocks.BLUE_ICE
    );

    public FallingBlockEntityMixin(EntityType<?> type, World world) {
        super(type, world);
    }

    private void crushIf(boolean setting, Map<Block, Block> progression, Block block_below, BlockPos pos_below) {
        if (!setting || !progression.containsKey(block_below)) {
            return;
        }
        world.breakBlock(pos_below, false);
        world.setBlockState(pos_below, progression.get(block_below).getDefaultState(), 3);
    }

    @Inject(method = "tick", locals = LocalCapture.CAPTURE_FAILHARD, at = @At(
            value = "FIELD",
            target = "Lnet/minecraft/entity/FallingBlockEntity;destroyedOnLanding:Z",
            shift = At.Shift.BEFORE
    ))
    private void onTick(CallbackInfo ci, Block falling_block, BlockPos landing_pos, boolean b1, boolean bl2, BlockState blockState_1) {
        if (getBlockState().isIn(BlockTags.ANVIL)) {
            var block_below = this.world.getBlockState(new BlockPos(this.getX(), this.getY() - 0.06, this.getZ())).getBlock();
            var pos_below = landing_pos.down();
            crushIf(AnvilCrushSettings.anvilCrushIce, crushIce, block_below, pos_below);
            crushIf(AnvilCrushSettings.anvilCrushStone, crushStone, block_below, pos_below);
        }
    }
}
