package technicalma.carpet.anvilcrush.mixins;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.ItemStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import technicalma.carpet.anvilcrush.helpers.AxeEnchantmentHelper;

@Mixin(Enchantment.class)
public abstract class EnchantmentMixin {
    @Inject(method = "isAcceptableItem", at = @At("RETURN"), cancellable = true)
    private void isAcceptableItem(ItemStack stack, CallbackInfoReturnable<Boolean> cir) {
        if (AxeEnchantmentHelper.isWeaponAxe(stack.getItem())) {
            Enchantment thisEnchantment = (Enchantment) (Object) this;

            for (var weaponEnchantment : AxeEnchantmentHelper.WEAPON_ENCHANTMENTS) {
                if (weaponEnchantment == thisEnchantment) {
                    cir.setReturnValue(true);
                    return;
                }
            }
        }
    }
}
