package technicalma.carpet.anvilcrush.mixins;

import net.minecraft.block.AnvilBlock;
import net.minecraft.block.BlockState;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import technicalma.carpet.anvilcrush.AnvilCrushSettings;

@Mixin(AnvilBlock.class)
public abstract class AnvilBlockMixin {
    @Inject(method = "getLandingState(Lnet/minecraft/block/BlockState;)Lnet/minecraft/block/BlockState;", at = @At("RETURN"), cancellable = true)
    private static void getLandingState(BlockState fallingState, CallbackInfoReturnable<BlockState> cir) {
        if (!AnvilCrushSettings.anvilFallingDamage) {
            cir.setReturnValue(fallingState);
        }
    }
}
