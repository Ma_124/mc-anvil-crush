package technicalma.carpet.anvilcrush.helpers;

import net.minecraft.block.ComposterBlock;
import net.minecraft.item.ItemConvertible;
import net.minecraft.item.Items;
import technicalma.carpet.anvilcrush.AnvilCrushSettings;

public class CompostHelper {
    private static void updateCompost(boolean enabled, ItemConvertible item, float prob) {
        if (enabled)
            ComposterBlock.ITEM_TO_LEVEL_INCREASE_CHANCE.put(item, prob);
        else
            ComposterBlock.ITEM_TO_LEVEL_INCREASE_CHANCE.removeFloat(item);
    }

    public static void updateCompostables() {
        // Same as fungi, roots and melon
        updateCompost(AnvilCrushSettings.compostRottenFlesh, Items.ROTTEN_FLESH, .65F);

        // Same as normal potatoes
        updateCompost(AnvilCrushSettings.compostPoisonousPotatoes, Items.POISONOUS_POTATO, .65F);
    }
}
