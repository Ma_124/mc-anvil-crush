package technicalma.carpet.anvilcrush.helpers;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.item.AxeItem;
import net.minecraft.item.Item;
import technicalma.carpet.anvilcrush.AnvilCrushSettings;

public class AxeEnchantmentHelper {
    public static final Enchantment[] WEAPON_ENCHANTMENTS = {
            Enchantments.SHARPNESS,
            Enchantments.SMITE,
            Enchantments.BANE_OF_ARTHROPODS,
            Enchantments.LOOTING,
            Enchantments.KNOCKBACK,
            Enchantments.FIRE_ASPECT
    };

    public static boolean isWeaponAxe(Item item) {
        return AnvilCrushSettings.axesAreWeapons && item instanceof AxeItem;
    }
}
