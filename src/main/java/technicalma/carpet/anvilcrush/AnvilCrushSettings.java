package technicalma.carpet.anvilcrush;

import carpet.settings.Rule;

import static carpet.settings.RuleCategory.*;

public class AnvilCrushSettings {
    public static final String ANVIL = "anvil";
    public static final String DAMAGE = "damage";
    public static final String MA = "Ma_124";

    private static final String REQUIRES_RELOAD = "changes require server restart or /reload";

    @Rule(
            desc = "falling anvils crush stony types",
            extra = "cobblestone -> gravel -> sand",
            category = {MA, ANVIL, SURVIVAL, EXPERIMENTAL, FEATURE}
    )
    public static boolean anvilCrushStone = false;

    @Rule(
            desc = "falling anvils crush ice",
            extra = "frosted ice -> ice -> packed ice -> blue ice",
            category = {MA, ANVIL, SURVIVAL, EXPERIMENTAL, FEATURE}
    )
    public static boolean anvilCrushIce = false;

    @Rule(
            desc = "falling anvils take damage",
            extra = "",
            category = {MA, ANVIL, SURVIVAL, EXPERIMENTAL}
    )
    public static boolean anvilFallingDamage = true;

    @Rule(
            desc = "villager lock trades.",
            extra = "this can permanently alter the behaviour of any villages traded with while this rule was activated",
            category = {MA, EXPERIMENTAL, SURVIVAL}
    )
    public static boolean villagerLockTrades = true;

    @Rule(
            desc = "players don't get hurt by fire",
            extra = "specifically applies to fire, lava, and magma blocks",
            category = {MA, SURVIVAL, DAMAGE}
    )
    public static boolean noFireDamage = false;

    @Rule(
            desc = "players don't get hurt by flying into walls",
            extra = "",
            category = {MA, SURVIVAL, DAMAGE}
    )
    public static boolean noKinecticDamage = false;

    @Rule(
            desc = "players don't get hurt by explosions",
            extra = "",
            category = {MA, SURVIVAL, DAMAGE}
    )
    public static boolean noExplosionDamage = false;

    @Rule(
            desc = "print coordinates in player death messages",
            extra = "",
            category = {MA, SURVIVAL}
    )
    public static boolean printDeathCoords = false;

    @Rule(
            desc = "compostable rotten flesh",
            extra = REQUIRES_RELOAD,
            category = {MA, SURVIVAL}
    )
    public static boolean compostRottenFlesh = false;

    @Rule(
            desc = "compostable poisonous potatoes",
            extra = REQUIRES_RELOAD,
            category = {MA, SURVIVAL}
    )
    public static boolean compostPoisonousPotatoes = false;

    @Rule(
            desc = "click through item frames that are invisible and fixed",
            extra = "",
            category = {MA, BUGFIX}
    )
    public static boolean clickThroughItemFrame = false;

    @Rule(
            desc = "shift + use (right mouse) makes item frames invisible and fixed",
            extra = "",
            category = {MA, CREATIVE, SURVIVAL, FEATURE}
    )
    public static boolean shiftClickItemFrameInvisible = false;

    @Rule(
            desc = "disable 'too expensive' on anvils",
            extra = "the UI will still show the error",
            category = {MA, SURVIVAL, ANVIL}
    )
    public static boolean anvilTooExpensive = true;

    @Rule(
            desc = "treat axes as weapons (e.g. allow looting in survival)",
            extra = "",
            category = {MA, SURVIVAL}
    )
    public static boolean axesAreWeapons = false;

    @Rule(
            desc = "zombified piglin no long spawn in nether portals",
            extra = "",
            category = {MA, SURVIVAL, CREATIVE}
    )
    public static boolean noPortalTicking = false;

    @Rule(
            desc = "keep experience points on death",
            extra = "",
            category = {MA, SURVIVAL}
    )
    public static boolean keepXP = false;

    @Rule(
            desc = "enable /where{is,ami}, /imhere, /{he,she}is for non operators",
            extra ="",
            category = {MA, SURVIVAL, CREATIVE}
    )
    public static boolean nonOPWhereIs = false;

    @Rule(
            desc = "villagers can be leashed",
            extra ="",
            category = {MA, SURVIVAL}
    )
    public static boolean canLeashVillager = false;
}
