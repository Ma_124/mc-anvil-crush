package technicalma.carpet.anvilcrush;

import carpet.CarpetExtension;
import carpet.CarpetServer;
import carpet.settings.SettingsManager;
import com.mojang.brigadier.CommandDispatcher;
import net.fabricmc.api.ModInitializer;
import net.minecraft.command.CommandRegistryAccess;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.command.ServerCommandSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import technicalma.carpet.anvilcrush.commands.AnalyzeCommand;
import technicalma.carpet.anvilcrush.commands.PlayerLocationCommand;
import technicalma.carpet.anvilcrush.helpers.CompostHelper;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;

public class AnvilCrushServer implements CarpetExtension, ModInitializer {
    public static final Logger LOGGER = LogManager.getLogger(AnvilCrushServer.class);

    @Override
    public void onInitialize() {
        var ruleDumpPath = System.getProperty("technicalma.carpet.anvilcrush.dumpRules");
        if (ruleDumpPath != null && !ruleDumpPath.isEmpty()) {
            // dump rules to readme
            var oldOut = System.out;
            try {
                System.setOut(new PrintStream(new BufferedOutputStream(new FileOutputStream(ruleDumpPath))));

                var settingsManager = new SettingsManager("0.0.0", "anvil-crush", "Anvil Crush");
                settingsManager.parseSettingsClass(AnvilCrushSettings.class);
                settingsManager.printAllRulesToLog(AnvilCrushSettings.MA);

                System.out.flush();
                // exit with special code expected by build.gradle
                System.exit(93);
            } catch (Exception e) {
                System.setOut(oldOut);
                e.printStackTrace();
                System.exit(1);
            }
        } else {
            // start mod normally
            CarpetServer.manageExtension(new AnvilCrushServer());
        }
    }

    @Override
    public void registerCommands(CommandDispatcher<ServerCommandSource> dispatcher, CommandRegistryAccess commandBuildContext) {
        AnalyzeCommand.register(dispatcher);
        PlayerLocationCommand.registerAll(dispatcher);
    }

    @Override
    public void onGameStarted() {
        CarpetServer.settingsManager.parseSettingsClass(AnvilCrushSettings.class);
    }

    @Override
    public void onServerLoaded(MinecraftServer server) {
        CompostHelper.updateCompostables();
    }

    @Override
    public void onReload(MinecraftServer server) {
        CompostHelper.updateCompostables();
    }

    @Override
    public String version() {
        return CarpetExtension.super.version();
    }
}
