package technicalma.carpet.anvilcrush.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.entity.Entity;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.util.math.Vec3d;
import technicalma.carpet.anvilcrush.AnvilCrushSettings;

import java.util.List;
import java.util.stream.Collectors;

import static net.minecraft.command.argument.EntityArgumentType.*;
import static net.minecraft.command.argument.MessageArgumentType.getMessage;
import static net.minecraft.command.argument.MessageArgumentType.message;
import static net.minecraft.server.command.CommandManager.argument;
import static net.minecraft.server.command.CommandManager.literal;

public class PlayerLocationCommand {
    private final String command;
    private final boolean useOwnLocation;
    private final boolean broadcast;

    public PlayerLocationCommand(String command, boolean useOwnLocation, boolean broadcast) {
        this.command = command;
        this.useOwnLocation = useOwnLocation;
        this.broadcast = broadcast;
    }

    public static void registerAll(CommandDispatcher<ServerCommandSource> dispatcher) {
        new PlayerLocationCommand("whereami", true, false).register(dispatcher);
        new PlayerLocationCommand("whereis", false, false).register(dispatcher);
        new PlayerLocationCommand("imhere", true, true).register(dispatcher);
        new PlayerLocationCommand("heis", false, true).register(dispatcher);
        new PlayerLocationCommand("sheis", false, true).register(dispatcher);
    }

    private static Text formatBlockPosition(Vec3d pos) {
        return Text.of(String.format("[x:%d, y:%d, z:%d]", Math.round(pos.x), Math.round(pos.y), Math.round(pos.z)));
    }

    public void register(CommandDispatcher<ServerCommandSource> dispatcher) {
        LiteralArgumentBuilder<ServerCommandSource> builder = literal(command)
                .requires(p -> p.hasPermissionLevel(2) || AnvilCrushSettings.nonOPWhereIs);

        if (useOwnLocation) {
            builder = builder
                    .executes(c -> execute(c, false))
                    .then(argument("message", message())
                            .executes(c -> execute(c, true)));
        } else {
            builder = builder.then(
                    argument("entities", entities())
                            .executes(c -> execute(c, false))
                            .then(argument("message", message())
                                    .executes(c -> execute(c, true))));
        }

        dispatcher.register(builder);
    }

    private int execute(CommandContext<ServerCommandSource> c, boolean hasMessage) throws CommandSyntaxException {
        List<Text> names;
        List<Vec3d> positions;
        if (useOwnLocation) {
            names = List.of(c.getSource().getDisplayName());
            positions = List.of(c.getSource().getPosition());
        } else {
            var e = getEntities(c, "entities");
            names = e.stream().map(Entity::getDisplayName).collect(Collectors.toList());
            positions = e.stream().map(Entity::getPos).collect(Collectors.toList());
        }

        for (int i = 0; i < positions.size(); i++) {
            Vec3d pos = positions.get(i);
            Text name = names.get(i);

            MutableText msg;
            if (!broadcast && useOwnLocation) msg = Text.literal("You are at ");
            else {
                msg = name.copy();
                msg.append(" is at ");
            }

            msg.append(formatBlockPosition(pos));
            msg.append(". ");
            if (hasMessage) msg.append(getMessage(c, "message"));

            if (broadcast) c.getSource().getServer().getPlayerManager().broadcast(msg, false);
            else c.getSource().sendFeedback(msg, false);
        }

        return 1;
    }
}
