package technicalma.carpet.anvilcrush.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.Text;
import net.minecraft.util.math.BlockPos;
import technicalma.carpet.anvilcrush.AnvilCrushServer;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class AnalyzeCommand {
    private static boolean isRunning = false;

    public static void register(CommandDispatcher<ServerCommandSource> dispatcher) {
        LiteralArgumentBuilder<ServerCommandSource> cmd = CommandManager.literal("analyze")
                .then(CommandManager.argument("radius", IntegerArgumentType.integer(1))
                        .requires(p -> p.hasPermissionLevel(4))
                        .executes(c -> {
                            if (c.getSource().getEntity() == null) {
                                c.getSource().sendError(Text.literal("not an entity"));
                                return 0;
                            }

                            var start = LocalDateTime.now();
                            if (isRunning) {
                                c.getSource().sendError(Text.literal("already running"));
                                return 0;
                            }
                            isRunning = true;

                            var centerChunk = c.getSource().getEntity().getChunkPos();
                            var world = c.getSource().getWorld();
                            int radius = c.getArgument("radius", Integer.class) - 1;
                            int minY = world.getBottomY();
                            int maxY = world.getTopY();

                            HashSet<Block> interestingBlocks = new HashSet<>();
                            interestingBlocks.add(Blocks.COAL_ORE);
                            interestingBlocks.add(Blocks.DEEPSLATE_COAL_ORE);
                            interestingBlocks.add(Blocks.IRON_ORE);
                            interestingBlocks.add(Blocks.DEEPSLATE_IRON_ORE);
                            interestingBlocks.add(Blocks.COPPER_ORE);
                            interestingBlocks.add(Blocks.DEEPSLATE_COPPER_ORE);
                            interestingBlocks.add(Blocks.GOLD_ORE);
                            interestingBlocks.add(Blocks.DEEPSLATE_GOLD_ORE);
                            interestingBlocks.add(Blocks.REDSTONE_ORE);
                            interestingBlocks.add(Blocks.DEEPSLATE_REDSTONE_ORE);
                            interestingBlocks.add(Blocks.EMERALD_ORE);
                            interestingBlocks.add(Blocks.DEEPSLATE_EMERALD_ORE);
                            interestingBlocks.add(Blocks.LAPIS_ORE);
                            interestingBlocks.add(Blocks.DEEPSLATE_LAPIS_ORE);
                            interestingBlocks.add(Blocks.DIAMOND_ORE);
                            interestingBlocks.add(Blocks.DEEPSLATE_DIAMOND_ORE);
                            interestingBlocks.add(Blocks.NETHER_GOLD_ORE);
                            interestingBlocks.add(Blocks.NETHER_QUARTZ_ORE);
                            interestingBlocks.add(Blocks.ANCIENT_DEBRIS);
                            interestingBlocks.add(Blocks.END_PORTAL_FRAME);
                            interestingBlocks.add(Blocks.LAVA);
                            interestingBlocks.add(Blocks.WATER);
                            interestingBlocks.add(Blocks.SPAWNER);
                            interestingBlocks.add(Blocks.INFESTED_STONE);
                            interestingBlocks.add(Blocks.INFESTED_DEEPSLATE);

                            ArrayList<HashMap<Block, Integer>> results = new ArrayList<>();
                            for (int y = minY; y < maxY; y++)
                                results.add(new HashMap<>());

                            for (int chunkX = centerChunk.x - radius; chunkX <= centerChunk.x + radius; chunkX++)
                                for (int chunkZ = centerChunk.z - radius; chunkZ <= centerChunk.z + radius; chunkZ++) {
                                    var chunk = world.getChunk(chunkX, chunkZ);
                                    for (int x = 0; x < 16; x++)
                                        for (int y = minY; y < maxY; y++)
                                            for (int z = 0; z < 16; z++) {
                                                var block = chunk.getBlockState(new BlockPos(x, y, z)).getBlock();
                                                if (interestingBlocks.contains(block))
                                                    results.get(y).put(block, results.get(y).getOrDefault(block, 0) + 1);
                                            }
                                }

                            try {
                                var f = new FileOutputStream("/tmp/mc-results.csv");
                                for (int y = 0; y < results.size(); y++) {
                                    var layer = results.get(y);
                                    for (var e : layer.entrySet())
                                        f.write(String.format("%s,%s,%s\n", e.getKey().getTranslationKey(), y, e.getValue()).getBytes(StandardCharsets.UTF_8));
                                }
                            } catch (IOException e) {
                                AnvilCrushServer.LOGGER.catching(e);
                            }

                            radius++;
                            long chunkCount = (long) radius * radius;
                            long blockCount = chunkCount * 16 * 16 * (maxY - minY);

                            c.getSource().sendFeedback(
                                    Text.literal("Analysing ")
                                            .append(Long.toString(chunkCount))
                                            .append(" chunks (")
                                            .append(Double.toString(((double) blockCount) / 1_000_000D))
                                            .append(" million blocks) took ")
                                            .append(Long.toString(Duration.between(start, LocalDateTime.now()).getSeconds()))
                                            .append("s.")
                                    , false
                            );

                            isRunning = false;
                            return 1;
                        }));
        dispatcher.register(cmd);
    }
}
