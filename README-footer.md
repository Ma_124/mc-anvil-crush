## Anvil Crush Commands
### /analyze \<radius\>
For each layer in the world (in the given radius) output how many blocks were found on that height
to `/tmp/mc-results.csv`.

Example excerpt from `/tmp/mc-results.csv`:

```csv
block.minecraft.redstone_ore,10,11423
block.minecraft.water,10,265
block.minecraft.deepslate_diamond_ore,10,298
block.minecraft.copper_ore,10,1250
block.minecraft.deepslate_iron_ore,10,2928
block.minecraft.coal_ore,10,14556
block.minecraft.diamond_ore,10,1540
block.minecraft.redstone_ore,11,11549
block.minecraft.water,11,1191
block.minecraft.deepslate_diamond_ore,11,297
block.minecraft.copper_ore,11,1227
block.minecraft.deepslate_iron_ore,11,2954
block.minecraft.coal_ore,11,14361
block.minecraft.diamond_ore,11,1530
```

## Build Mod for different MC versions
Download the repository
- using git: `git clone https://gitlab.com/Ma_124/mc-anvil-crush.git`.
- as a [.zip file](https://gitlab.com/Ma_124/mc-anvil-crush/-/archive/master/mc-anvil-crush-master.zip).

If you want to build the mod yourself for a different Minecraft version you need to change the following files:
- [`src/main/resources/fabric.mod.json`](./src/main/resources/fabric.mod.json): Change `depends.minecraft` to new version.
- [`gradle.properties`](./gradle.properties)
  - Update the properties under `# Fabric Properties` with the values found [here](https://fabricmc.net/versions.html).
  - Update to the most recent carpet version for your release. A list of all carpet versions can be found [here](https://masa.dy.fi/maven/carpet/fabric-carpet).
- [`build.gradle`](./build.gradle): Under the `publishModrinth` task change `addGameVersion` to the appropriate version(s).

You may have to change the Java version which can be done at the following places:
- [`build.gradle`](./build.gradle):
  - Change `sourceCompatibility` and `targetCompatibility`.
  - Change `it.options.release` in `tasks.withType(JavaCompile)`.
- [`./src/main/resources/anvil-crush.mixins.json`](./src/main/resources/anvil-crush.mixins.json): Change `compatibilityLevel`.
